const btnUp = {
  el: document.querySelector('.up-link'),
  show() {
    this.el.classList.remove('up-link_hide');
  },
  hide() {
    this.el.classList.add('up-link_hide');
  },
  addEventListener() {
    window.addEventListener('scroll', () => {
      const scrollY = window.scrollY || document.documentElement.scrollTop;
      scrollY > 400 ? this.show() : this.hide();
    });
    document.querySelector('.up-link').onclick = () => {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }
  }
}

btnUp.addEventListener();