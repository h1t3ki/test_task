const themeToggle = document.querySelector("#theme-toggle");
const currentTheme = localStorage.getItem("theme"); 

function chosenTheme() {
  if (currentTheme == "dark") {
    document.body.classList.add("dark-theme");
  }
  if (currentTheme == "light") {
    document.body.classList.add("light-theme");
  }
}

themeToggle.addEventListener("click", function() {
  document.body.classList.toggle("dark-theme");
  let theme = "light";
  if (document.body.classList.contains("dark-theme")) {
    theme = "dark";
  }
  localStorage.setItem("theme", theme);
});
