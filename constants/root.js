'use strict'
const ROOT_HEADER = document.querySelector('#header');
const ROOT_SHOES = document.querySelector('#shoes');
const ROOT_JEANS = document.querySelector('#jeans');
const ROOT_SWEATSHIRTS = document.querySelector('#sweatshirts');
const ROOT_FORM = document.querySelector('#shopping-form');
const ROOT_FOOTER = document.querySelector('#footer-tags');