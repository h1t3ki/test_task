"use strict";
class Jeans {
  OpenShoppingPage() {
    shopingPage.render();
  }
  render() {
    let htmlJeans = "";

    JEANS.forEach(({ id, name, price, description, img, date }) => {
      let dateElem = new Date(date);
      let formatter = new Intl.DateTimeFormat("ru", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      });

      htmlJeans += `
          <li class="jeans-elem">
            <img class="jeans-elem__img" src="${img}"/>
            <span class="jeans-elem__name">${name}</span>
            <span class="jeans-elem__description">${description}</span>
            <span class="jeans-elem__date">Дата добавления на сайт: ${formatter.format(
              dateElem
            )}</span>
            <button class="jeans-elem__button" onclick="jeansPage.OpenShoppingPage();">🛒 ${price.toLocaleString()}₽</button>
          </li>
      `;
    });

    const html = `
          <h2 class="jeans-title">Джинсы</h2>
          <ul class="jeans-section">
              ${htmlJeans}
          </ul>
      `;

    ROOT_JEANS.innerHTML = html;
  }
}

const jeansPage = new Jeans();
jeansPage.render();
