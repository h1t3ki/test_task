"use strict";
class Shoping {
  handleClear() {
    ROOT_FORM.innerHTML = '';
  }
  handleAlert() {
    return alert("Куплено!");
  }
  render() {
    const html = `
    <form class="shopping-form">
      <div class="shopping-form__elem">
        <label class="shoping-form__span" for="quantity">Выберите количество</label>
        <input type="number" min="0" name="quantity" class="shopping-form__input" id="quantity"/>
      </div>
      <div class="shopping-form__elem shopping-form__elem_radio">
        <span class="shoping-form__span">Выберите цвет:</span>
        <input id="black" type="radio" class="shopping-form__radio" name="color">
        <label for="black" class="class="shopping-form__item shopping-form__item_black">Черный</label>
        <input id="white" type="radio" class="shopping-form__radio" name="color">
        <label for="white" class="class="shopping-form__item shopping-form__item_white">Белый</label>
        <input id="blue" type="radio" class="shopping-form__radio" name="color">
        <label for="blue" class="class="shopping-form__item shopping-form__item_blue">Синий</label>
        <input id="gray" type="radio" class="shopping-form__radio" name="color">
        <label for="gray" class="class="shopping-form__item shopping-form__item_gray">Серый</label>
      </div>
      <div class="shopping-form__elem">
        <span class="shoping-form__span">Оставьте комментарий:</span>
        <textarea rows="10" cols="45" name="text" maxlength="2000" class="shoping-form__textarea"></textarea>
      </div>
      <div class="shopping-form__elem">
        <button class="shoping-form__button" onclick="shopingPage.handleAlert()">Купить</button>
        <button class="shoping-form__button" onclick="shopingPage.handleClear()">Закрыть</button>
      </div>
    </form>
    `;
    ROOT_FORM.innerHTML = html;
  }
}

const shopingPage = new Shoping();
