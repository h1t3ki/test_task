"use strict";
class Sweatshirts {
  OpenShoppingPage() {
    shopingPage.render();
  }
  render() {
    let htmlSweatshirts = "";

    SWEATSHIRTS.forEach(({ id, name, price, description, img, date }) => {
      let dateElem = new Date(date);
      let formatter = new Intl.DateTimeFormat("ru", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      });

      htmlSweatshirts += `
          <li class="sweatshirts-elem">
            <img class="sweatshirts-elem__img" src="${img}"/>
            <span class="sweatshirts-elem__name">${name}</span>
            <span class="sweatshirts-elem__description">${description}</span>
            <span class="sweatshirts-elem__date">Дата добавления на сайт: ${formatter.format(
              dateElem
            )}</span>
            <button class="sweatshirts-elem__button" onclick="sweatshirtsPage.OpenShoppingPage();">🛒 ${price.toLocaleString()}₽</button>
          </li>
      `;
    });

    const html = `
          <h2 class="sweatshirts-title">Свитера</h2>
          <ul class="sweatshirts-section">
              ${htmlSweatshirts}
          </ul>
      `;

    ROOT_SWEATSHIRTS.innerHTML = html;
  }
}

const sweatshirtsPage = new Sweatshirts();
sweatshirtsPage.render();
