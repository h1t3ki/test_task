"use strict";
class Shoes {
  OpenShoppingPage() {
    shopingPage.render();
  }
  render() {
    let htmlShoes = "";

    SHOES.forEach(({ id, name, price, description, img, date }) => {
      let dateElem = new Date(date);
      let formatter = new Intl.DateTimeFormat("ru", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      });

      htmlShoes += `
          <li class="shoes-elem">
            <img class="shoes-elem__img" src="${img}"/>
            <span class="shoes-elem__name">${name}</span>
            <span class="shoes-elem__description">${description}</span>
            <span class="shoes-elem__date">Дата добавления на сайт: ${formatter.format(
              dateElem
            )}</span>
            <button class="shoes-elem__button" onclick="shoesPage.OpenShoppingPage();">🛒 ${price.toLocaleString()}₽</button>
          </li>
      `;
    });

    const html = `
          <h2 class="shoes-title">Обувь</h2>
          <ul class="shoes-section">
              ${htmlShoes}
          </ul>
      `;

    ROOT_SHOES.innerHTML = html;
  }
}

const shoesPage = new Shoes();
shoesPage.render();
