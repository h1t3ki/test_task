'use strict'
class Footer {

  render() {
    const html = `
        <div class="footer-container">
          <ul class="footer-nav">
            <li class="footer-nav__link"><a href="#shoes">Обувь</a></li>
            <li class="footer-nav__link"><a href="#jeans">Джинсы</a></li>
            <li class="footer-nav__link"><a href="#sweatshirts">Свитера</a></li>
          </ul>
        </div>
    `;
    ROOT_FOOTER.innerHTML = html;
  }
}

const footerPage = new Footer();
footerPage.render();
