'use strict'
class Header {

  render() {
    const html = `
        <div class="header-container">
          <ul class="header-nav">
            <li class="header-nav__link"><a href="#shoes">Обувь</a></li>
            <li class="header-nav__link"><a href="#jeans">Джинсы</a></li>
            <li class="header-nav__link"><a href="#sweatshirts">Свитера</a></li>
          </ul>
          <button id="theme-toggle" class="theme-toggle">Сменить тему</button>
        </div>
    `;
    ROOT_HEADER.innerHTML = html;
  }
}

const headerPage = new Header();
headerPage.render();
